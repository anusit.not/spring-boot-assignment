# Assignment-one
### คำแนะนำ
> สำหรับพนักงานที่เข้ามาร่วมทีมใหม่ เพื่อเป็นการทดสอบความสามารถเบื้องต้น และการฝึกฝนเพื่อให้เกิดความชำนาญและความเข้าใจ 
ในส่วนงานที่เกี่ยวข้องกับการเขียนโปรแกรมโดยการใช้งาน Spring boot ทางทีมงานจึงมีแนวคิดที่จะสร้างหลักสูตรเร่งรัด เพื่อให้พนักงานใหม่ได้เข้าใจโครงสร้างของ Coding รวมไปถึง Stacks ต่าง ๆ ที่ใช้ในโปรเจ็ค 
ดังนั้นจึงได้ออกแบบบททดสอบนี้ขึ้นมาโดยเป็นมีปัญหาที่ต้องดำเนินการแก้ไขทั้งหมด 20 ข้อ พนักงานสามารถใช้ Git repository เดียวกันเพื่อแก้โจทย์ปัญหาทั้ง 20 ข้อ โดยอาจจะแตกเป็น Feature branch ของแต่ละข้อก็ได้เช่นกัน เช่น
feature-question-1 เสร็จแล้วทำการ merge กลับไปที่ branch master 
### หมายเหตุ
> ก่อนที่จะทำแบบทดสอบ พนักงานต้องผ่านการอ่านและศึกษาข้อมูลใน Teams Group [CPX-BE] ที่เป็นเอกสารและ Video ทั้งหมดให้เรียบร้อย
> ระยะเวลาในการทำแบบทดสอบนี้คือ 5 วัน สามารถทำเสร็จก่อนเวลาได้

### วิธีการส่งคำตอบ [นำไปใช้อ้างอิงในการส่งคำตอบของแต่ละข้อ]
1. capture screen short ของโปรแกรมขณะที่ทำงานเพื่อแสดงให้เห็นว่าโปรแกรมสามารถทำงานได้ถูกต้อง [ans:01]
2. capture screen short ของ postman กรณีที่จำเป็นต้องทดสอบด้วย postman เพื่อแสดง request + response [ans:02]
3. capture screen short ของ database กรณีที่ใช้งานร่วมกับ database เพื่อแสดงการเปลี่ยนแปลงของข้อมูล [ans:03]
4. push code ขึ้นไป gitlab ที่ https://gitlab.backendteam.xyz/[first-name-last-name] [ans:04]

> ส่งคำตอบมาที่ cpxbackendteams@scbcorp.onmicrosoft.com โดยแยกเป็นข้อ ๆ กรณี 1 ข้อมีมากกว่าหนึ่งรูปสามารถแยกดังนี้  
- 1.1.jpg
- 1.2.jpg
- 2.1.jpg
- ......
- 20.1.jpg 

### package และโปรแกรมที่จะนำมาใช้งานในแบบทดสอบ
- lombok 
- flyway 
- mockito 
- postgresql
- kafka
## แบบทดสอบ 
#### ข้อที่ 1 
- ให้สร้างโปรเจ็ค Spring boot โดยใช้ spring initializr ต้องเลือก package ที่ระบุไว้ด้านบนและสามารถลง package อื่น ๆ ได้ หลังจากสร้างเสร็จให้ทำการ push code ไปที่ gitlab ตามลิงค์ที่แนบไว้ด้านบน
ผลลัพธ์ที่คาดหวัง สามารถ run project spring-boot รวมถึงสามารถใช้งาน git ได้
- คำตอบที่ต้องส่ง : ref [ans:01]

#### ข้อที่ 2 
- ให้สร้าง Controller file พร้อมทั้ง return ข้อความออกทางหน้าจอว่า `Hello Backend Team`
- คำตอบที่ต้องส่ง : ref [ans:01]

#### ข้อที่ 3
- ให้ทำการรัน docker-compose ไฟล์ของ postgrest sql เพื่อทดสอบการเชื่อมต่อกับฐานข้อมูล โดยเมื่อสามารถเชื่อต่อได้แล้วให้ทำการสร้างฐานข้อมูลชื่อ `svc_backend_team`
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:03]

#### ข้อที่ 4 
- ให้ทำการเชื่อต่อ spring-boot application เข้ากับ database postgrest sql กับฐานข้อมูล `svc_backend_team`
- คำตอบที่ต้องส่ง : ref [ans:01]

#### ข้อที่ 5
- ให้สร้าง Flyway migration file โดยให้ Flyway ทำหน้าที่สร้าง table `user` ภายใต้ฐานข้อมูล `svc_backend_team` ในส่วนของชื่อ field สามารถออกแบบได้ตามจิตนาการ
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:03]

#### ข้อที่ 6 
- ให้สร้าง File Service , Repository, Service , Mapper, Model ขึ้นมาภายในโปรเจ็คและเชื่อมโยงแต่ละไฟล์เข้าด้วยกันโดยให้โปรแกรมสามารถทำงานได้
- คำตอบที่ต้องส่ง : ref [ans:01]

#### ข้อที่ 7 [Create Data] [Flyway, lombok]
- ให้สร้างฟังก์ชั่นในการสร้างข้อมูล User โดยจะต้องใช้ HTTP method POST ส่งข้อมูลผ่านทาง JSON body
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] ,[ans:03]

#### ข้อที่ 8 [Find By Id] [Flyway, lombok]
- ให้สร้างฟังก์ชั่นในการเรียกดูข้อมูลรายไอดี User โดยจะต้องใช้ HTTP method GET ส่งข้อมูล ID ผ่านทาง uri /{id} 
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] ,[ans:03]

#### ข้อที่ 9 [Get All Data][Flyway, lombok]
- ให้สร้างฟังก์ชั่นในการเรียกดูข้อมูลรายกรายทั้งหมดของ User โดยจะต้องใช้ HTTP method GET  
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] ,[ans:03]  

#### ข้อที่ 10 [Update all Data by id] [Flyway, lombok]
- ให้สร้างฟังก์ชั่นในการแก้ไขข้อมูลรายไอดี User โดยจะต้องใช้ HTTP method PUT ส่งข้อมูล ID ผ่านทาง uri /{id} 
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] ,[ans:03]

#### ข้อที่ 11 [Update partial Data by id] [Flyway, lombok]
- ให้สร้างฟังก์ชั่นในการแก้ไขข้อมูลบางส่วนรายไอดี User โดยจะต้องใช้ HTTP method PATCH ส่งข้อมูล ID ผ่านทาง uri /{id} 
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] ,[ans:03]  

#### ข้อที่ 12 [Delete Data by id] [Flyway, lombok]
- ให้สร้างฟังก์ชั่นในการแก้ไขข้อมูลบางส่วนรายไอดี User โดยจะต้องใช้ HTTP method PATCH ส่งข้อมูล ID ผ่านทาง uri /{id} 
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] ,[ans:03]

### ข้อที่ 13.1 [Kafka]
- ให้ทำการรัน docker-compose ไฟล์ของ kafka และทำการเชื่อมต่อ kafka เข้ากับ spring-boot project
- คำตอบที่ต้องส่ง : ref [ans:01] 

### ข้อที่ 13.2 [Kafka topic]
 - ให้สร้างฟังก์ชั่นในการส่ง topic message ออกจาก spring boot project ไปยัง kafka
 - คำตอบที่ต้องส่ง : ref [ans:01] 

### ข้อที่ 13.3 [Kafka listener | worker | consumer]
- ให้สร้างฟังก์ชั่นในการรับ message จาก topic จาก kafka server
- คำตอบที่ต้องส่ง : ref [ans:01] 

### ข้อที่ 14 [Error handling | Validation]
- ให้เพิ่มฟังก์ชั่นในการ Validation ข้อมูลใน Controller ก่อนจะส่งข้อมูลไปประมวลผลที่ Service หากพบว่าข้อมูลไม่ตรงตาม spec ที่ออกแบบไว้ให้ return code 422 กลับไป เช่น api required email แต่ไม่ส่ง email ระบบจะ return 422
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] 

### ข้อที่ 15 [Error handling | sql exception]  
- ให้เขียนฟังก์ชั่นตรวจสอบข้อมูล กรณีที่ไม่สามารถทำงานร่วมกับ database ได้มาอย่างน้อย 1 กรณี
- คำตอบที่ต้องส่ง : ref [ans:01] ,[ans:02] 

### ข้อที่ 16 [Unit test for controller] [mockito]
- ให้เขียน unit สำหรับไฟล์ Controller ทุกไฟล์ที่เขียนขึ้นมาใน project
- คำตอบที่ต้องส่ง : ref [ans:01] * ภาพหน้าจอ test result

### ข้อที่ 17 [Unit test for service] [mockito]
- ให้เขียน unit สำหรับไฟล์ Service ทุกไฟล์ที่เขียนขึ้นมาใน project
- คำตอบที่ต้องส่ง : ref [ans:01] * ภาพหน้าจอ test result

### ข้อที่ 18 [Unit test for repository] [mockito]
- ให้เขียน unit สำหรับไฟล์ Repository ทุกไฟล์ที่เขียนขึ้นมาใน project
- คำตอบที่ต้องส่ง : ref [ans:01] * ภาพหน้าจอ test result

### ข้อที่ 19 [Open idea] 
- ให้เขียนฟังกช์พิเศษที่ต้องการนำเสนออะไรก็ได้เช่น cache, batch, thread, cloud ,redis ,memcache, elastic ,log หรืออื่น ๆ อะไรก็ได้ที่อยากจะเขียนเพื่อแสดงความสามารถของตัวเองออกมา
- คำตอบที่ต้องส่ง : ref [ans:01] 

### ข้อที่ 20 [Git]
- ให้ merge code เข้า branch master แล้ว push code และทุก feature branch ไปยัง remote repository ของตัวเอง 
- คำตอบที่ต้องส่ง : ref [ans:04] 

# คำแนะนำอื่น ๆ 
#### Api restFull standard method ประกอบไปด้วย 
- Get /  byId , lists 
- Post 
- Delete 
- Patch (update partial)
- Put (update replace)
โดย url จะมีรูปแบบ 
- {host}/api/v1/{entity} 

#### โครงสร้างของ code ตามมาตรฐานที่วางไว้ใน 1 task จะประกอบไปด้วยไฟล์
- Controller 
- Service 
- Repository 
- Mapper 
- Data

#### จะต้องทำ unit test ให้ code coverage >= 80 %

### ขั้นตอนการเตรียมเครื่องมือ
- สมัคร account gitlab ที่ https://gitlab.backendteam.xyz/
- สร้าง repository ของตัวเอง
- ดู code ตัวอย่างจาก https://gitlab.backendteam.xyz/backend-team/spring-boot-ecommerce/ เพื่อนำมาเป็น idea
- kafka docker compose https://github.com/wurstmeister/kafka-docker
- postgrest && pgAdmin https://github.com/khezen/compose-postgres
