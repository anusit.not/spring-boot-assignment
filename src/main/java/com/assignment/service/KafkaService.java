package com.assignment.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class KafkaService {
	private static String TOPIC = "kafka";
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public void sendMessage(String message) {
		try {
			log.info("Send Message: "+TOPIC+" "+ message);
			this.kafkaTemplate.send(TOPIC, message);
		} catch (Exception e) {
			log.error("msg error : " + e);
		}
	}

//	@KafkaListener(topics = "kafka", groupId = "myGroup")
//	public void receiveMessage(String message) throws IOException {
//		log.info("Receive Message: "+ message);
//	}
}
