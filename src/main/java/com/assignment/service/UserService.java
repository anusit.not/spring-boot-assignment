package com.assignment.service;

import com.assignment.exception.BusinessException;
import com.assignment.model.UserModel;
import com.assignment.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserService {

    @Autowired
    UserRepository userRepository;

    public UserModel createUser(UserModel user) {
        String userId = userRepository.createUser(user);
        UserModel userDetail;
        if (userId == null) {
            throw new BusinessException(500, "can't create user");
        } else {
            userDetail = userRepository.findUserById(userId);
        }
        return userDetail;
    }

    public UserModel findUserById(String id) throws BusinessException {
        UserModel userDetail = userRepository.findUserById(id);
        return userDetail;
    }

    public List<UserModel> findAllUser() throws BusinessException {
        return userRepository.findAllUser();
    }

    public UserModel updateUserAllById(UserModel user, String id)  throws BusinessException{
        if(userRepository.updateUserAllById(user, id) == null) {
            throw new BusinessException(500, "can't update user");
        }
        return userRepository.findUserById(id);
    }

    public Boolean updateEmailById(UserModel user, String id)  throws BusinessException{
        if(!userRepository.updateEmailById(user, id)) {
            throw new BusinessException(500, "can't update user");
        }
        return userRepository.updateEmailById(user, id);
    }

    public Boolean deleteUser(String id) throws BusinessException {
        Boolean deleteUser = userRepository.deleteUser(id);
        if(!deleteUser) {
            throw new BusinessException(500, "can't delete user");
        }
        return deleteUser;
    }

    public String updateUsernameById(UserModel user, String id){
        return userRepository.updateUsernameById(user, id);
    }
}

