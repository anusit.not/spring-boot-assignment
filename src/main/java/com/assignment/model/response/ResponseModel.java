package com.assignment.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ResponseModel<T> implements Serializable {
    private int statusCode = 200;
    private String statusHeader = "success";
    private String statusDescription = "";
    @JsonProperty("data")
    private T data;

    public ResponseModel(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseModel(T data) {
        this.data = data;
    }

    public ResponseModel(int statusCode, String statusHeader) {
        this.statusCode = statusCode;
        this.statusHeader = statusHeader;
    }

    public ResponseModel(int statusCode, String statusHeader, T data) {
        this.statusCode = statusCode;
        this.statusHeader = statusHeader;
        this.data = data;
    }

    public ResponseModel(int statusCode, String statusHeader, String statusDescription) {
        this.statusCode = statusCode;
        this.statusHeader = statusHeader;
        this.statusDescription = statusDescription;
    }
}
