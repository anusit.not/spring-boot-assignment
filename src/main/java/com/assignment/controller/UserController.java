package com.assignment.controller;

import com.assignment.model.UserModel;
import com.assignment.model.response.ResponseModel;
import com.assignment.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody UserModel user) throws NoSuchAlgorithmException {
        UserModel userDetail = userService.createUser(user);
        if (user.getUsername().isEmpty() || user.getPassword().isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseModel<ArrayList<Object>>(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(),HttpStatus.BAD_REQUEST.getReasonPhrase()));
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseModel(userDetail));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findUserById(@PathVariable("id") String id) {
        UserModel user = userService.findUserById(id);
        ResponseModel<UserModel> data = new ResponseModel<>(user);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(data);
    }

    @GetMapping("")
    public ResponseEntity<?> findAllUser() {
        List<UserModel> userList = userService.findAllUser();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseModel(userList));
    }

    @PutMapping(" {id}")
    public ResponseEntity<?> updateUserAllById(@RequestBody UserModel user, @PathVariable("id") String id) throws NoSuchAlgorithmException {
        UserModel userDetail = userService.updateUserAllById(user, id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseModel(userDetail));
    }

    @PatchMapping("/updateEmail/{id}")
    public ResponseEntity<?> updateEmailById(@RequestBody UserModel user, @PathVariable("id") String id) throws NoSuchAlgorithmException {
        userService.updateEmailById(user, id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseModel(HttpStatus.OK.value()));
    }

    @PatchMapping("/delete/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable("id") String id) {
        UserModel user = userService.findUserById(id);
        if(user == null){
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ResponseModel(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase()));
        }
        userService.deleteUser(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseModel(HttpStatus.OK.value()));
    }

    @PatchMapping("/updateUsername/{id}")
    public ResponseEntity<?> updateUsernameById(@RequestBody UserModel user, @PathVariable("id") String id) throws NoSuchAlgorithmException {
        String res = userService.updateUsernameById(user, id);
        if(!res.equals("")){
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseModel(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),res));
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ResponseModel(HttpStatus.OK.value()));
    }
}
