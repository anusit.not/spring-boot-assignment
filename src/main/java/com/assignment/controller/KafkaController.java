package com.assignment.controller;

import com.assignment.model.response.ResponseModel;
import com.assignment.service.KafkaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/v1/kafka")
@Slf4j
public class KafkaController {
	@Autowired
	KafkaService KafkaService;

	@GetMapping("/sendMessage/{message}")
	public ResponseEntity<?> sendMessage(@PathVariable("message") String message) {
		KafkaService.sendMessage(message);
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(new ResponseModel(HttpStatus.OK.value()));

	}
}
