package com.assignment.repository;

import com.assignment.mapper.UserMapper;
import com.assignment.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.repository.query.QueryCreationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;
import java.util.UUID;

import static com.assignment.helper.EncryptionHelper.md5;

@Repository
@Slf4j
public class UserRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public String createUser(UserModel user) {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("INSERT INTO users(").add("id, username, password, first_name, last_name, email, created_datetime, updated_datetime)")
                .add("VALUES (:id, :username, :password, :first_name, :last_name, :email, now(), now());");


        HashMap<String, Object> params = new HashMap<>();
        String userId = UUID.randomUUID().toString();
        params.put("id", userId);
        params.put("username", user.getUsername());
        params.put("password", md5(user.getPassword()));
        params.put("first_name", user.getFirstName());
        params.put("last_name", user.getLastName());
        params.put("email", user.getEmail());
        try {
            jdbcTemplate.update(sql.toString(), params); 
            return userId;
        } catch (DataIntegrityViolationException ex) {
            return null;
        }
    }

    public UserModel findUserById(String userId) {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("select id, username, first_name, last_name, email, created_datetime, updated_datetime from users")
                .add("where id =:userId");

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", userId);

        try {
            return jdbcTemplate.queryForObject(sql.toString(), params, new UserMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error("not found user");
            return null;
        }
    }

    public List<UserModel> findAllUser() {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("select id, username, first_name, last_name, email, created_datetime, updated_datetime from users");

        try {
            List<UserModel> rs = jdbcTemplate.query(sql.toString(), new UserMapper());
            return rs;
        } catch (EmptyResultDataAccessException ex) {
            log.error("not found user");
            return null;
        }
    }

    public Object updateUserAllById(UserModel user, String id) {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("UPDATE users ")
                .add("SET username = :username, ")
                .add("    first_name = :first_name, ")
                .add("    last_name = :last_name,  ")
                .add("    email = :email, ")
                .add("    updated_datetime = now() ")
                .add("WHERE id = :id;");
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("username", user.getUsername());
        params.put("first_name", user.getFirstName());
        params.put("last_name", user.getLastName());
        params.put("email", user.getEmail());
        try {
            return jdbcTemplate.update(sql.toString(), params);
        } catch (Exception ex) {
            return null;
        }
    }

    public Boolean updateEmailById(UserModel user, String id) {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("UPDATE users ")
                .add("SET email = :email, ")
                .add("    updated_datetime = now() ")
                .add("WHERE id = :id;");
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("email", user.getEmail());
        try {
            jdbcTemplate.update(sql.toString(), params);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public Boolean deleteUser(String id) {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("DELETE FROM users ")
                .add("where id = :id");

        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        try {
            jdbcTemplate.update(sql.toString(), params);
            return true;
        } catch (QueryCreationException ex) {
            log.error("query error", ex.getMessage());
            return false;
        }
    }

    public String updateUsernameById(UserModel user, String id) {
        String res = "";
        StringJoiner sql = new StringJoiner(" ");
        sql.add("UPDATE users ")
                .add("SET username = :username, ")
                .add("    updated_datetime = now() ")
                .add("WHERE id = :id;");
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("username", user.getUsername());
        try {
            jdbcTemplate.update(sql.toString(), params);
        } catch (Exception ex) {
            ex.printStackTrace();
            res = ex.getMessage();
        }
        return res;
    }
}
