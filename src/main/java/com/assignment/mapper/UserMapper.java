package com.assignment.mapper;

import com.assignment.model.UserModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<UserModel> {
    @Override
    public UserModel mapRow(ResultSet resultSet, int i) throws SQLException {
        UserModel user = new UserModel();
        user.setId(resultSet.getString("id"));
        user.setUsername(resultSet.getString("username"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setEmail(resultSet.getString("email"));
        user.setCreatedDatetime(resultSet.getString("created_datetime"));
        user.setUpdatedDatetime(resultSet.getString("updated_datetime"));
        return user;
    }
}
