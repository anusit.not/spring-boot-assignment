--DROP TABLE users;

CREATE TABLE users (
    id character varying(60) NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(50) NOT NULL,
    first_name character varying(100) ,
    last_name character varying(100) ,
    email character varying(100) ,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    CONSTRAINT user_pk PRIMARY KEY (id)
);