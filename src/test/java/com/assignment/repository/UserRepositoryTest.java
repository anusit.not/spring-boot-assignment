package com.assignment.repository;

import com.assignment.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

import static com.assignment.helper.EncryptionHelper.md5;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        StringJoiner sql = new StringJoiner(" ");
        sql.add("INSERT INTO users(")
                .add("id, username, password, first_name, last_name, email, created_datetime, updated_datetime)")
                .add("VALUES ('test01', 'unit.test01', '25d55ad283aa400af464c76d713c07ad', 'test', 'repository', 'unit_test@gmail.com', now(), now());");

        jdbcTemplate.execute(sql.toString());
    }

    @Test
    public void createUserSuccess() {
        UserModel mockUser = new UserModel();
        mockUser.setUsername("unit.test");
        mockUser.setPassword("12345678");
        mockUser.setFirstName("unit");
        mockUser.setLastName("test");
        mockUser.setEmail("unit.test@gmail.com");

        String userId = userRepository.createUser(mockUser);
        assertNotNull(userId);
    }

    @Test
    public void createUserFail() {
        UserModel mockUser = new UserModel();
        mockUser.setUsername(null);
        mockUser.setPassword("12345678");
        mockUser.setFirstName("unit");
        mockUser.setLastName("test");
        mockUser.setEmail("unit.test@gmail.com");

        String userId = userRepository.createUser(mockUser);
        assertNull(userId);
    }

    @Test
    public void findUserByIdSuccess() {
        String id = "test01";
        UserModel user = userRepository.findUserById(id);
        assertNotNull(user);
        assertEquals("test01", user.getId());
        assertEquals("unit.test01", user.getUsername());
    }

    @Test
    public void findUserByIdFail() {
        String id = "000";
        UserModel user = userRepository.findUserById(id);
        assertNull(user);
    }

    @Test
    public void findAllUserSuccess() {
        List<UserModel> userList = userRepository.findAllUser();
        assertNotNull(userList);
    }

    @Test
    public void updateUserAllByIdSuccess() {
        String id = "test01";
        UserModel mockUser = new UserModel();
        mockUser.setUsername("unit.test");
        mockUser.setFirstName("unit");
        mockUser.setLastName("testRepo");
        mockUser.setEmail("unit.test@gmail.com");

        Object user = userRepository.updateUserAllById(mockUser, id);
        assertNotNull(user);
    }

    @Test
    public void updateUserAllByIdFail() {
        String id = "test01";
        UserModel mockUser = new UserModel();
        mockUser.setUsername(null);
        mockUser.setFirstName("unit");
        mockUser.setLastName("testRepo");
        mockUser.setEmail("unit.test@gmail.com");

        Object user = userRepository.updateUserAllById(mockUser, id);
        assertNull(user);
    }

    @Test
    public void updateEmailByIdSuccess() {
        String id = "test01";
        UserModel mockUser = new UserModel();
        mockUser.setEmail("unit.test2020@gmail.com");

        Boolean user = userRepository.updateEmailById(mockUser, id);
        assertTrue(user);
    }
    @After
    @Test
    public void deleteUserSuccess() {
        String id = "test01";

        Boolean user = userRepository.deleteUser(id);
        assertTrue(user);
    }

    @Test
    public void updateUsernameByIdSuccess() {
        String id = "test01";
        UserModel mockUser = new UserModel();
        mockUser.setUsername("test.00");

        String user = userRepository.updateUsernameById(mockUser,id);
        assertEquals("", user);
    }

    @Test
    public void updateUsernameByIdFail() {
        String id = "test01";
        UserModel mockUser = new UserModel();
        mockUser.setUsername(null);

        String user = userRepository.updateUsernameById(mockUser,id);
        assertNotEquals("", user);
    }
}
