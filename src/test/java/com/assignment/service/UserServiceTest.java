package com.assignment.service;

import com.assignment.exception.BusinessException;
import com.assignment.model.UserModel;
import com.assignment.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void createUserSuccess() {
        UserModel mockUser = new UserModel();
        mockUser.setId(UUID.randomUUID().toString());
        mockUser.setUsername("unit.test");
        mockUser.setPassword("12345678");
        mockUser.setFirstName("unit");
        mockUser.setLastName("test");
        mockUser.setEmail("unit.test@gmail.com");

        doReturn(mockUser.getId())
                .when(userRepository)
                .createUser(any());
        doReturn(mockUser)
                .when(userRepository)
                .findUserById(mockUser.getId());
        UserModel user = userService.createUser(mockUser);
        assertEquals(user, mockUser);
    }

    @Test
    public void createUserFailCanNotCreateUser() {
        UserModel mockUser = new UserModel();
        mockUser.setId(UUID.randomUUID().toString());
        mockUser.setUsername("unit.test");
        mockUser.setPassword("12345678");
        mockUser.setFirstName("unit");
        mockUser.setLastName("test");
        mockUser.setEmail("unit.test@gmail.com");

        doThrow(new BusinessException(500, "can't create user"))
                .when(userRepository)
                .createUser(mockUser);
        try {
            userService.createUser(mockUser);
        } catch (BusinessException ex) {
            assertEquals("500", ex.getCode());
            assertEquals("can't create user", ex.getMessage());
        }
    }

    @Test
    public void findUserByIdSuccess() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";
        UserModel mockUser = new UserModel();
        doReturn(mockUser)
                .when(userRepository)
                .findUserById(id);
        UserModel user = userService.findUserById(id);
        assertEquals(user, mockUser);
    }

    @Test
    public void findAllUserSuccess() {
        List<UserModel> mockUser = new ArrayList<>();
        doReturn(mockUser)
                .when(userRepository)
                .findAllUser();
        List<UserModel> user = userService.findAllUser();
        assertEquals(user, mockUser);
    }

    @Test
    public void updateUserAllByIdSuccess() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";
        UserModel mockUser = new UserModel();
        mockUser.setUsername("test.update");
        mockUser.setPassword("12345678");
        mockUser.setFirstName("test");
        mockUser.setLastName("update");
        mockUser.setEmail("unit.test@gmail.com");

        doReturn(mockUser)
                .when(userRepository)
                .updateUserAllById(mockUser, id);
        doReturn(mockUser)
                .when(userRepository)
                .findUserById(id);
        UserModel user = userService.updateUserAllById(mockUser, id);
        assertEquals(user, mockUser);
    }

    @Test
    public void updateUserAllByIdFailCanNotUpdateUser() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";
        UserModel mockUser = new UserModel();
        mockUser.setUsername("test.update");
        mockUser.setPassword("12345678");
        mockUser.setFirstName("test");
        mockUser.setLastName("update");
        mockUser.setEmail("unit.test@gmail.com");

        doThrow(new BusinessException(500, "can't update user"))
                .when(userRepository)
                .updateUserAllById(mockUser, id);
        try {
            userService.updateUserAllById(mockUser, id);
        } catch (BusinessException ex) {
            assertEquals("500", ex.getCode());
            assertEquals("can't update user", ex.getMessage());
        }
    }

    @Test
    public void updateEmailByIdSuccess() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";
        UserModel mockUser = new UserModel();
        mockUser.setEmail("unit.test@gmail.com");

        doReturn(new Boolean(true))
                .when(userRepository)
                .updateEmailById(mockUser, id);
        Boolean res = userService.updateEmailById(mockUser, id);
        assertTrue(res);
    }

    @Test
    public void updateEmailByIdFailCanNotUpdateUser() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";
        UserModel mockUser = new UserModel();
        mockUser.setEmail("unit.test@gmail.com");

        doThrow(new BusinessException(500, "can't update user"))
                .when(userRepository)
                .updateEmailById(mockUser, id);
        try {
            userService.updateEmailById(mockUser, id);
        } catch (BusinessException ex) {
            assertEquals("500", ex.getCode());
            assertEquals("can't update user", ex.getMessage());
        }
    }

    @Test
    public void deleteUserSuccess() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";

        doReturn(new Boolean(true))
                .when(userRepository)
                .deleteUser(id);
        Boolean res = userService.deleteUser(id);
        assertTrue(res);
    }

    @Test
    public void deleteUserFailCanNotDeleteUser() {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";

        doThrow(new BusinessException(500, "can't delete user"))
                .when(userRepository)
                .deleteUser(id);
        try {
            userService.deleteUser(id);
        } catch (BusinessException ex) {
            assertEquals("500", ex.getCode());
            assertEquals("can't delete user", ex.getMessage());
        }
    }

}
