package com.assignment.controller;

import com.assignment.exception.BusinessException;
import com.assignment.model.UserModel;
import com.assignment.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.assignment.BaseTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest extends BaseTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void createUserSuccess() throws Exception {
        UserModel user = new UserModel();
        user.setUsername("unit.test");
        user.setPassword("12345678");
        user.setFirstName("unit");
        user.setLastName("test");
        user.setEmail("unit.test@gmail.com");

        doReturn(new UserModel())
                .when(userService)
                .createUser(any());
        this.testPostController("/v1/users/create", user, "200");
    }

    @Test
    public void createUserFail() throws Exception {
        UserModel user = new UserModel();
        user.setUsername("");
        user.setPassword("");
        user.setFirstName("unit");
        user.setLastName("test");
        user.setEmail("unit.test@gmail.com");

        doReturn(new UserModel())
                .when(userService)
                .createUser(user);
        this.testPostController("/v1/users/create", user, "400");
    }

    @Test
    public void findUserByIdSuccess() throws Exception {
        String id = "a15a451e-ec82-4dae-99c8-297580323069";

        doReturn(new UserModel())
                .when(userService)
                .findUserById(id);
        this.testGetController("/v1/users/".concat(id), null, "200");
    }

    @Test
    public void findAllUserSuccess() throws Exception {
        doReturn(new ArrayList<UserModel>())
                .when(userService)
                .findAllUser();
        this.testGetController("/v1/users", null, "200");
    }

    @Test
    public void updateUserAllByIdSuccess() throws Exception {
        String id = "23607b66-29a2-4e38-8e56-f7c9635eadd9";
        UserModel user = new UserModel();
        user.setUsername("unit");
        user.setPassword("test1234");
        user.setFirstName("unit");
        user.setLastName("test");
        user.setEmail("unit.test@gmail.com");

        this.testPutController("/v1/users/update/".concat(id), user, "200");
    }

    @Test
    public void updateEmailByIdSuccess() throws Exception {
        String id = "23607b66-29a2-4e38-8e56-f7c9635eadd9";
        UserModel user = new UserModel();
        user.setEmail("unit.test@gmail.com");

        this.testPatchController("/v1/users/updateEmail/".concat(id), user, "200");
    }

    @Test
    public void deleteUserByIdSuccess() throws Exception {
        String id = "1dcff06d-d381-475b-a79b-1af39dea3c41";
        doReturn(new UserModel())
                .when(userService)
                .findUserById(id);
        this.testPatchController("/v1/users/delete/".concat(id), null, "200");
    }

    @Test
    public void deleteUserByIdNotFoundUser() throws Exception {
        String id = "testNoDate1234";
        this.testPatchController("/v1/users/delete/".concat(id), null, "404");
    }

    @Test
    public void updateUsernameByIdFailSqlException() throws Exception {
        String id = "1dcff06d-d381-475b-a79b-1af39dea3c41";
        UserModel user = new UserModel();
        user.setUsername(null);
        doReturn(new UserModel().toString())
                .when(userService)
                .updateUsernameById(user,id);
        this.testPatchController("/v1/users/updateUsername/".concat(id), user, "500");
    }
}
